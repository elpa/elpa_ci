#!/bin/sh

modes=("after" "before" "generated_tests" "hip_on_nvidia_tests" "layout_tests" "nvhpcsdk_tests" "project_tests" "special_tests" "sycl_on_cpu_tests")

for mode in "${modes[@]}"; do
  rm "../${mode}.inc.yml"
  python3 generate_gitlab_ci_tests.py --mode=${mode} > "../${mode}.inc.yml"
done
